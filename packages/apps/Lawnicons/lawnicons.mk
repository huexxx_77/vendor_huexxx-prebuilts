# Lawnchair
LAWN_ROOT := vendor/huexxx-prebuilts/packages/apps/Lawnicons

PRODUCT_PACKAGES += Lawnicons
PRODUCT_COPY_FILES += \
    $(LAWN_ROOT)/permissions/privapp-permissions-lawnicons.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-lawnicons.xml \
    $(LAWN_ROOT)/sysconfig/lawnicons-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/sysconfig/lawnicons-hiddenapi-package-whitelist.xml
